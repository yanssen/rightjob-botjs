import fetch from 'node-fetch';
import express from "express";
import * as dotenv from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
dotenv.config()


var mongoDbUrl = process.env.MONGODB_URL || "mongodb://127.0.0.1:27017"; //by default

//*****************************/
// Generate access token 
const tokenURL = "https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=";
const realm = "/partenaire";
const content_type = "application/x-www-form-urlencoded";
const grant_type ="client_credentials";
// scope api_[Identifiant de l'API][Version] scope api_[API identifier][Version] Allows your application to access the corresponding API (if several APIs, separate these scopes by spaces) api_labonneboitev1
const scope = "api_offresdemploiv2";

// put in environment variables the id and the secret of the client ?
const client_id = "PAR_syrh_53ad60a897a11d2854b3875d056b9e5269b35b5c417ed270fb0da5b2138f7133";
const client_secret = "4d582fc2a39d15271077498a05ffb81cc4c7b5472500849f29e6c0eec3982d4e";
var accessToken="";

//*****************************/
// Connect to the api with an access token
const apiURL = "https://api.emploi-store.fr/partenaire";
const apiName="offresdemploi";
const apiVersion="v2";

const app = express();

// test route
app.get("/test",async (req,res)=>{
    const data = await generateAccessTokenFetch();
    console.log(data);
    res.json(data);
});

async function generateAccessTokenFetch() {
    console.log(" test ");
    try {
        const response = await fetch(tokenURL+realm,{
            method: "post",
            headers: {"Content-Type": content_type},
            body: {"grant_type" : grant_type,"client_id":client_id,"client_secret":client_secret,"scope":scope},
        })
        if (!response.ok) {
          const message = 'Error with Status Code: ' + response.status;
          throw new Error(message);
        }
        const data = await response.json();
        console.log(data);
      } catch (error) {
        console.log('Error: ' + err);
      }
      return data;
}

app.listen(3000);


