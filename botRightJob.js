import fetch from 'node-fetch';
import * as dotenv from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
dotenv.config();

import mongoose from 'mongoose';
import dbMongoose from './db-mongoose.js';
var thisDb = dbMongoose.thisDb;

//NB: This is for current entity type ("Devise" or "Customer" or "Product" or ...)
//NB: thisSchema end ThisPersistentModel should not be exported (private only in this current module)
var thisSchema;//mongoose Shcema (structure of mongo document)
var ThisPersistentModel; //mongoose Model (constructor of persistent ThisPersistentModel)


//*****************************/
// Generate access token 
const tokenURL = process.env.TOKEN_URL;
const realm = process.env.REALM;
const content_type = process.env.CONTENT_TYPE;
const grant_type = process.env.GRANT_TYPE;
// scope api_[Identifiant de l'API][Version] scope api_[API identifier][Version] Allows your application to access the corresponding API (if several APIs, separate these scopes by spaces) api_labonneboitev1
const scope = "api_offresdemploiv2";

// put in environment variables the id and the secret of the client ?
const client_id = process.env.CLIENT_ID;
const client_secret =  process.env.CLIENT_SECRET;
var accessToken="";

//*****************************/
// Connect to the api "offresdemploi" with the generated access token
const apiURI =  process.env.API_URI;
const apiName=process.env.API_NAME;
const apiVersion=process.env.API_VERSION;



async function generateAccessTokenFetch() {
    console.log(" test ");
    try {
        const response = await fetch(tokenURL+realm,{
            method: "post",
            headers: {"Content-Type": content_type},
            body: {"grant_type" : grant_type,"client_id":client_id,"client_secret":client_secret,"scope":scope},
        })
        if (!response.ok) {
          const message = 'Error with Status Code: ' + response.status;
          throw new Error(message);
        }
        const data = await response.json();
        console.log(data);
      } catch (error) {
        console.log('Error: ' + err);
      }
      return data;
}

//accessToken=generateAccessTokenFetch();


function initMongooseWithSchemaAndModel () {
  mongoose.Connection = thisDb;
      thisSchema = new mongoose.Schema({
        action: { type : String } ,
        who: { type : String}         
      });

      ThisPersistentModel = mongoose.model('test', thisSchema);

}
initMongooseWithSchemaAndModel();

function reinit_db(){
  return new Promise( (resolve,reject)=>{
      const deleteAllFilter = { }
      ThisPersistentModel.deleteMany( deleteAllFilter, function (err) {
        if(err) { 
          console.log(JSON.stringify(err));
          reject(err);
        }
        //insert elements after deleting olds
        (new ThisPersistentModel({ action : "Hello" , who : "World "+new Date()})).save();
        (new ThisPersistentModel({ action : "Slt" , who : "Monde "+new Date()})).save();
        resolve({action:"test collection re-initialized in mongoDB database"})
      })
  });
}
reinit_db()
.then((value) => {
  console.log(value);
})
.catch(console.error)
.finally(()=>  process.exit());