import mongoose from 'mongoose';
import * as dotenv from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
dotenv.config()

var mongoDbUrl = process.env.MONGODB_URI; 
//ou bien "mongodb://superuser:motdepasse@127.0.0.1:27017"


console.log("mongoDbUrl="+mongoDbUrl);
mongoose.connect(mongoDbUrl, {useNewUrlParser: true, 
	                              useUnifiedTopology: true , 
								  dbName : 'poleEmploitoRJDb'});
var thisDb  = mongoose.connection;

thisDb.on('error' , function() { 
      console.log("mongoDb connection error = " + " for dbUrl=" + mongoDbUrl )
    });

thisDb.once('open', function() {
      // we're connected!
      console.log("Connected correctly to mongodb database" );
    });

export default { thisDb } ;

